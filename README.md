<img src=".github/GWpyxel.png" width="auto" height="200" alt="Cover Image">

# GWpyxel

![GitHub Latest Release: 0.0.1)](https://img.shields.io/badge/Release-v0.0.1-green?logo=github)
![Made in Python 3.9.0](https://img.shields.io/badge/Python-3.9.0-blue)
[![Under MIT license](https://img.shields.io/badge/License-MIT-yellow)](LICENSE)


Adapting machine learning pipeline for burst gravitational waves searches to pulsar glitches.

Developed by Maxime Fays and Vincent Boudart.

## Table of contents
- [About the pipeline](#about-the-pipeline)
- [Installation](#installation)
- [How to run](#how-to-run)
- [Roadmap](#roadmap)
- [FAQ](#faq)
- [Contributing](#contributing)
- [License](#license)

## About the pipeline
Minute-long gravitational-wave burst, arising from diverse astrophysical phenomena, are poorly modeled. To tackle this challenge, GWpyxel introduces a convolutional neural network that ensures pixel-wise precision without relying on strong assumptions. The method involves searching for a local excess of power in the time-frequency space correlated between detectors.

For more information, please check the following articles:
- ["A machine learning algorithm for minute-long Burst searches"](https://arxiv.org/abs/2201.08727), Vincent Boudart, Maxime Fays - arXiv:2201.08727 (2022)
- ["ALBUS: a machine learning algorithm for gravitational wave burst searches"](https://ieeexplore.ieee.org/document/10020896), Vincent Boudart, Maxime Fays - DOI: 10.1109/BigData55660.2022.10020896

## Installation
The following instructions are for GNU/Linux x86 64 bits

### Instructions on CIT

```bash
conda create --clone igwn --name GWpyxel-O4a
conda activate GWpyxel-O4a
```

Cloning iwgn takes about 30min. 

```bash
git clone https://git.ligo.org/maxime.fays/gwpyxel.git
cd ~/gwpyxel/
pip install -e .
```

### Instructions on local machine

```bash
brew install krb5
brew install swig
brew install openssl
conda install -c conda-forge pyfftw
conda install -c conda-forge nds2-client 
conda install -c conda-forge nds2-client-swig python-nds2-client

pip install ciecplib

# on OSX:
install Xcode (via app store)

python -m ipykernel install --user --name=firstEnv
```

```bash
conda create --name GWpyxel python=3.9
conda activate GWpyxel
git clone https://git.ligo.org/maxime.fays/gwpyxel.git ~/gwpyxel
cd ~/gwpyxel/
pip install -e .

conda install nds2-client -c conda-forge
conda install nds2-client-swig python-nds2-client -c conda-forge
```

### Activate GWpyxel

```bash
conda activate GWpyxel
```

## How to run
Example

```bash
GWpyxel --H1L1 1242369180.0 --output ~/gwpyxel_output/
GWpyxel --V1L1 1242369180.0 --output ~/gwpyxel_output/

GWpyxel --H1 1242380191.0 --L1 1242369180.0 --output ~/gwpyxel_output/
GWpyxel --H1 1242380191.0 --L1 1242369180.0 --output ~/gwpyxel_output/ --plot
GWpyxel --H1 1242380191.0 --L1 1242369180.0 --output ~/gwpyxel_output/ --plot --tfmap_stride 6.0 --tfmap_fftlen 0.5 --tfmap_snr 8.0
GWpyxel --H1 1242380191.0 --L1 1242369180.0 --output ~/gwpyxel_output/ --plot --tfmap_stride 6.0 --tfmap_fftlen 0.5 --tfmap_snr 8.0 --sim_waveform maXgnetarD --sim_hrss 2e-21 
```

Additional input parameters available with :

```bash
GWpyxel --help
```

Pre-baked configurations can be used with the --config options. The options supports external files as well. 

Examples to call from inside python are available in the Notebook folder.

### Training ALBUS

```bash
conda activate GWpyxel
mkdir test_review
cd test_review
```

Generate the .dag and .sub for the 4 datasets : "background", "chirps", "glitches" and "combined".

```bash
../gwpyxel/GWpyxel/bin/GWpyxel_generate_training --run O3a --ifos H1L1 --background_amount 10000 --chirps_amount 1000 --glitches_amount 500000 --combined_amount 1000
```

Select some gliches across a wide variety of classes and SNR.
```bash
python3 ../gwpyxel/GWpyxel/bin/select_GS_glitches.py 
```

Run the .dag for the "background" and "chirps" datasets.
```bash
cd background
condor_submit_dag background.dag

cd ../chirps
condor_submit_dag chirps.dag
```

Train alpha. It will be used to find glitches in the cross-correlated TF maps.
```bash
cd ..
../gwpyxel/GWpyxel/bin/GWpyxel_train_alpha --Nb_epochs 30 --batch_size 32 --background_amount 5000 --chirps_amount 5000
```
```bash
cd glitches
condor_submit_dag glitches.dag
```
Select some glitches to be part of the "combined" dataset. Then, remove them from the "glitches" dataset.
```bash
cd ..
python3 ../gwpyxel/GWpyxel/bin/generate_list_of_glitches.py
```
Run the combined.dag to generate our last dataset.
```bash
cd combined
condor_submit_dag combined.dag
```
Finally train ALBUS on the 4 generated datasets.
```bash
cd ..
../gwpyxel/GWpyxel/bin/GWpyxel_train_ALBUS --Nb_epochs 30 --batch_size 32 
```
## FAQ

If you cannot download the data, make sure that you are inputing your LIGO credential when asked to generate a Kerberos ticket.

### Frangi got an unpexcted argument 'mode'

This happens when the version of scikit-image installed through pip is not the latest one for some reason. Fix with :

```bash
pip install --upgrade scikit-image
```

## Roadmap
- [x] Get familiar with code
- [ ] Start with single-detector SFTs and consider multi-detector setups later
- [ ] Split the frequency band into narrow sub-bands
- [ ] Create spectrograms with the x-axis representing timestamps of SFTsand the y-axis representing frequency bins for each sub-band.
- [ ] Check if we get a similar total pixel count as they've optimised their network for, from longer x-axis but narrow band in y
- [ ] Check if it works finding roughly the right signal strength ballpark (known sky location, small overall freq range, no higher spindowns...)
- [ ] Check in real data

## Contributing
To get started contributing, make sure to read the [Contributing Guide](.github/CONTRIBUTING.md) before making a issue or a pull request.

## License
The source code for the site is licensed under the MIT license, which you can find in the [LICENSE](LICENSE) file.