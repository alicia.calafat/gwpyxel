#!/usr/bin/env python3
import configargparse
import pandas as pd
import GWpyxel
from GWpyxel.const import PATH_TO_PYXEL

def parse_arguments():
    parser = configargparse.ArgParser()
    parser.add('--era', required=False, type=str, default='O4')
    return parser.parse_args()

def load_glitches(era):
    return pd.read_csv(f'{PATH_TO_PYXEL}/glitches/{era}/gspy_{era}.csv')

def select_glitches(glitches_df, ifo, classes, snr_range, n_selected):
    selected_glitches = pd.DataFrame()
    for glitch in classes:
        for i, snr_min in enumerate(snr_range[:-1]):
            snr_max = snr_range[i+1]
            found = glitches_df.loc[(glitches_df['ifo'] == ifo) &
                                    (glitches_df['label'] == glitch) &
                                    (glitches_df['snr'] < snr_max) &
                                    (glitches_df['snr'] > snr_min)]
            if len(found) >= n_selected:
                selected = found.sample(n=n_selected)
                selected_glitches = pd.concat([selected_glitches, selected])
    return selected_glitches

def save_selected_glitches(glitches, era, ifo):
    glitches.to_csv(f'{PATH_TO_PYXEL}/glitches/{era}/{ifo}_glitches.csv')

def main():
    options = parse_arguments()
    gspy = load_glitches(options.era)

    # Query somme glitches from different classes with a variety of SNRs
    classes = ['Blip', 'Low_Frequency_Burst', 'Scattered_Light', 'Tomte', 'Whistle', 
           'Extremely_Loud', 'Koi_Fish', 'Power_Line', 'Violin_Mode', 'Air_Compressor', 
           'Repeating_Blips', '1400Ripples', 'None_of_the_Above', '1080Line', 'Helix', 'Paired_Doves', 'Scratchy', ]

    snr_range = [20, 30, 40, 50, 100, 150, 200, 300, 500, 10000]
    n_selected = 30

    H1_glitches = select_glitches(gspy, 'H1', classes, snr_range, n_selected)
    L1_glitches = select_glitches(gspy, 'L1', classes, snr_range, n_selected)

    save_selected_glitches(H1_glitches, options.era, 'H1')
    save_selected_glitches(L1_glitches, options.era, 'L1')

if __name__ == "__main__":
    main()




