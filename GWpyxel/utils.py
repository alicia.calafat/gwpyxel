# import pyxel utils function
import configparser
import numpy as np
import pandas as pd
import copy
import pathlib
from yaspin import yaspin
from datetime import datetime
from termcolor import colored
import configdot

from .const import PATH_TO_PYXEL


try:
    from collections.abc import Iterable
except ImportError:
    from collections import Iterable
    
def flatten(items):
    """Yield items from any nested iterable; see Reference."""
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, (str, bytes)):
            for sub_x in flatten(x):
                yield sub_x
        else:
            yield x


def iter_loadtxt(filename, delimiter=",", skiprows=0, dtype=float):
    def iter_func():
        with open(filename, "r") as infile:
            for _ in range(skiprows):
                next(infile)
            for line in infile:
                if not line.startswith("%"):
                    line = line.rstrip().split(delimiter)
                    for item in line:
                        yield dtype(item)
        iter_loadtxt.rowlength = len(line)

    data = np.fromiter(iter_func(), dtype=dtype)
    data = data.reshape((-1, iter_loadtxt.rowlength))
    return data


def next_power_of_2(x):
    return 1 if x == 0 else 2 ** (x - 1).bit_length()


import os
import uuid


def generate_submit_file(dataframe, filename, output, executable, target_maps=False):
    output = os.path.abspath(output)

    # ---- write DAG
    with open(f"{output}/{filename}.dag", "w") as myfile:
        for i, row in dataframe.iterrows():
            unique_id = str(uuid.uuid4()).replace("-", "")
            job_string = f"pyxel_{filename}_{unique_id}"
            construct_job = "JOB " + job_string + f" {filename}.sub\n"
            construct_job += "RETRY " + job_string + " 2\n"
            construct_job += (
                "VARS "
                + job_string
                + " "
                + "".join(
                    map(
                        'macroargument{0}="{1}" '.format,
                        range(len(row.keys())),
                        map("--{0} {1}".format, row.keys(), row.values),
                    )
                )
                + "\n"
            )
            myfile.write(construct_job)

    # ---- write SUB
    submit_file = []
    submit_file.append("universe = vanilla")
    submit_file.append(f"executable = {PATH_TO_PYXEL}/bin/{executable}")
    submit_file.append("getenv = True")
    submit_file.append("priority = 0")
    submit_file.append("request_memory = 3000")
    submit_file.append("request_disk= 0.1M")
    submit_file.append(f"accounting_group_user = {os.getenv('USER')}")
    submit_file.append("accounting_group = ligo.dev.o4.burst.allsky_ld.xoffline")
    submit_file.append(f"log = /local/{os.getenv('USER')}/run1.log")
    submit_file.append(f"error = logs/xmerge-$(cluster)-$(process).err")
    submit_file.append(f"output = logs/xmerge-$(cluster)-$(process).out")
    submit_file.append("notification = Complete")
    submit_file.append(f"notify_user = {os.getenv('USER')}@ligo.org")
    submit_file.append(
        "PeriodicRemove = ( JobStatus == 2 ) && ( ( CurrentTime - EnteredCurrentStatus ) > 1800 )"
    )
    submit_file.append("queue 1")

    macroargs = " ".join(
        map("$(macroargument{0})".format, range(len(dataframe.keys())))
    )

    if(target_maps==False):
        submit_file.insert(
            4,
            f"arguments = {macroargs} --output {output}/output/ --config {output}/config.ini",
        )
    else:
        submit_file.insert(
            4,
            f"arguments = {macroargs} --output {output}/output/ --output_target_maps {output}/output_target_maps/ --config {output}/config.ini",
        )

    pathlib.Path(f'{output}/output/').mkdir(parents=True, exist_ok=True)

    with open(f"{output}/{filename}.sub", "w") as outfile:
        for line in submit_file:
            outfile.write(line + "\n")

def mask_percent(pyxel,percent=0.5):
    ht = pyxel.simulation.mask_ht
    Fs = pyxel.data.H1.sample_rate.value

    vsum = []
    for aa in np.array_split(np.abs(ht.value),int(pyxel.config.data.duration/pyxel.config.tfmap.tres)):
        vsum.append(np.sum(aa,axis=0))
       
    vmask = np.zeros_like(vsum)
    i_start = np.argmax(vsum)
    vmask[i_start] = 1
    total_energy = np.sum(vsum)

    while np.sum(vsum[np.min(np.nonzero(vmask)[0]):np.max(np.nonzero(vmask)[0])]) < percent*total_energy:
        left = np.min(np.nonzero(vmask)[0])-1
        right = np.max(np.nonzero(vmask)[0])+1
        if vsum[left] >= vsum[right]:
            vmask[left] = 1
        else:
            vmask[right] = 1

    tf_mask = copy.copy(pyxel.simulation.mask)
    tf_mask[:np.min(np.nonzero(vmask)[0]),:] = 0
    tf_mask[np.max(np.nonzero(vmask)[0])+2:,:] = 0

    return tf_mask

import numpy as np
import pandas as pd


#%%
#Functions to convert RegionProps to a Pandas dataframe

# Make list of all non-dunder attributes of regionprops object
def scalar_attributes_list(im_props):
    """
    Makes list of all scalar, non-dunder, non-hidden
    attributes of skimage.measure.regionprops object
    """
    
    attributes_list = []
    
    for i, test_attribute in enumerate(dir(im_props[0])):
        
        #Attribute should not start with _ and cannot return an array
        #does not yet return tuples
        if test_attribute[:1] != '_' and not\
                isinstance(getattr(im_props[0], test_attribute), np.ndarray):                
            attributes_list += [test_attribute]
            
    return attributes_list


def regionprops_to_df(im_props):
    """
    Read content of all attributes for every item in a list
    output by skimage.measure.regionprops
    """

    attributes_list = scalar_attributes_list(im_props)

    # Initialise list of lists for parsed data
    parsed_data = []

    # Put data from im_props into list of lists
    for i, _ in enumerate(im_props):
        parsed_data += [[]]
        
        for j in range(len(attributes_list)):
            parsed_data[i] += [getattr(im_props[i], attributes_list[j])]

    # Return as a Pandas DataFrame
    return pd.DataFrame(parsed_data, columns=attributes_list)

def run_at_gps(gps):
    import configparser
    # Initialize configparser
    era = configparser.ConfigParser()
    era.read(f"{PATH_TO_PYXEL}/runs/era.ini")

    # Construct gps_ranges dynamically from the era.ini file
    gps_ranges = {}
    for section in era.sections():
        start = int(era.get(section, 'start'))
        end = int(era.get(section, 'end'))
        gps_ranges[(start, end)] = section

    if type(gps) is str:
        for key, value in gps_ranges.items():
            if value == gps:
                logger.info(f"\t\t  GPS time  {key[0]}-{key[1]} inferred from run {gps}")
                return [key[0], key[1]]
    else:
        for (start, end), run in gps_ranges.items():
            if start <= gps <= end:
                logger.info(f"\t\t  {run} inferred from GPS time {gps}")
                return run

    logger.critical(f"\t\t  Could not find a run during the GPS time {gps}")
    return

import os
import configdot
from termcolor import colored
import time
import logging

logger = logging.getLogger(__name__)

def read_ini_file(config,verbose=False):
    """
    Read and load the specified config file.
    The function looks interatively in the following paths, stopping after 
    the first match:
        1. in the current directory (if the input is a filename)
        2. at the input location (if the input contains path)
        3. in the 'config' directory of GWpyxel
        4. if all above fails, load the default configuration 

    Args:
    - config (str): Relative or full path to a config file
    - verbose (bool, optional): If True, write a message when the config file is not found.

    Returns:
    - configdot 
    """
    start_time = time.time()

    # Define the search paths
    search_paths = [
        os.path.join(os.getcwd(), config),
        config,
        os.path.join(PATH_TO_PYXEL, "config", config),
        os.path.join(PATH_TO_PYXEL, "config", "default.ini")
    ]

    # Look for the config file
    for ii,path in enumerate(search_paths):
        if os.path.isfile(path):
            config_path = path
            break
        else:
            if verbose:
                # Write a message to the spinner if the config file was not found and verbose is True.
                logger.info(f"\t\t   Config NOT found in {path}")
    
    # Load the config file from the path found
    conf = configdot.parse_config(config_path)
    from pathlib import Path
    conf.cache.path = Path(conf.cache.path).expanduser()
    # Get the time taken to read the config file and format it as a string.
    end_time = time.time()
    elapsed_time = end_time - start_time


    if ii ==len(search_paths)-1:
        # Set the spinner text to indicate that the _default_ config file is being used.
        logger.warning(f"({elapsed_time:.3f}s)  {colored('Warning: Using default config file at','light_red')} {config_path}")
    else:
        # Set the spinner text to indicate that the config file was found.
        logger.info(f"({elapsed_time:.3f}s)  Found config file at {config_path}")

    # Set the spinner icon to indicate that the operation was successful.

    return conf


# ---- Define a timer to be used with Yaspin
class TimedText:
    def __init__(self, text):
        self.text = text
        self._start = datetime.now()

    def __str__(self):
        now = datetime.now()
        delta = now - self._start
        return f"{self.text} ({round(delta.total_seconds(), 3)}s)"

import time
import logging

logger = logging.getLogger(__name__)
message = None

def set_logging_message(new_message):
    global message
    message = new_message

def time_logger(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        elapsed_time = end_time - start_time
        message_to_log = message if message is not None else f"Function {func.__name__} executed"
        logger.info(f"\t ({elapsed_time:.3f}s) {message_to_log}")
        return result
    return wrapper