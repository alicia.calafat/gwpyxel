
import warnings
warnings.filterwarnings("ignore", "Wswiglal-redir-stdio")
import lal

from . import utils
from . import processing
from . import const
from . import condor
from .MLA import compute_map

import os
import logging
from termcolor import colored

# Get the value of the GWPYXEL_DEBUG_LEVEL environment variable, defaulting to INFO if not set
debug_level = os.environ.get("GWPYXEL_DEBUG_LEVEL", "WARNING")


# Configure the root logger based on the debug_level
logging.basicConfig(level=debug_level)  # Set the logging level based on the environment variable

# Customize the root logger's output format
formatter = logging.Formatter('[%(levelname)s] \t %(message)s')  # Customize the log format here
for handler in logging.root.handlers:
    handler.setFormatter(formatter)

# Log a message using the root logger
logging.info(f"\t\t   Starting GWpyxel")

from . import const
from .processing import *
from .condor import *
#from .utils import *
from . import pyxel
