import configparser
import logging
from GWpyxel.utils import time_logger
import pandas as pd

logger = logging.getLogger(__name__)

@time_logger
def segments_to_blocks(segments, block_duration):
    # Initial creation of the DataFrame with the correct columns
    coinc_table = pd.DataFrame(segments, columns=['start', 'end'])
    coinc_table['duration'] = coinc_table['end'] - coinc_table['start']

    analysis_segments_data = []
    leftover_segments_data = []

    for _, row in coinc_table.iterrows():
        n_full = int(row['duration'] // block_duration)
        leftover = row['duration'] % block_duration

        for n in range(n_full):
            # Directly append data with required structure
            analysis_segments_data.append({
                'H1': row['start'] + (n * block_duration),  # H1 is same as start for analysis segments
                'L1': row['start'] + (n * block_duration),  # L1 is also same as start (adjust if necessary)
                'duration': block_duration
            })

        if leftover > 0 and n_full > 0:
            final_block_start = row['end'] - block_duration
            final_block_end = row['end']
            skip_first_x_seconds = block_duration - leftover
            # Append leftover segments with additional detail
            leftover_segments_data.append({
                'H1': final_block_start,  # H1 is same as start for leftover segments
                'L1': final_block_start,  # L1 is also same as start (adjust if necessary)
                'duration': block_duration,
                'skip_first_x_seconds': skip_first_x_seconds
            })
        # The case for segments shorter than block_duration is already handled by not creating blocks for them

    analysis_segments = pd.DataFrame(analysis_segments_data)
    leftover_segments = pd.DataFrame(leftover_segments_data)
    
    return analysis_segments, leftover_segments
