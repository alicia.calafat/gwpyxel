import logging
logger = logging.getLogger(__name__)

from GWpyxel.MLA import compute_map
import os
import numpy as np

_USE_INI_DEFAULT = object()

from GWpyxel.utils import time_logger
from GWpyxel.utils import set_logging_message
@time_logger

def run_albus(self, version=2):
    self.save_plot_albus(version=version)

    if version == 2:
        loc_map, glitch_map, AS, GS, input_torch = compute_map.compute_map(f"{self.name}.png", version=version)
    elif version=='test':
        loc_map, glitch_map, AS, GS, input_torch = compute_map.compute_map(f"{self.name}.png", version=version)
    os.remove(f"{self.name}.png")
    
    TFmap = np.flipud(np.swapaxes(loc_map,0,1))
    Glitch = np.flipud(np.swapaxes(glitch_map,0,1))

    TFmap[TFmap<0] = 0
    Glitch[Glitch<0] = 0
    
    #TFmap = resize(TFmap, (self.spectrogram.shape[0], self.spectrogram.shape[1]),
    #               anti_aliasing=False,preserve_range=True)
    #Glitch = resize(Glitch, (self.spectrogram.shape[0], self.spectrogram.shape[1]),
    #               anti_aliasing=False,preserve_range=True)        

    self.TFmap = TFmap
    self.Glitch = Glitch
    set_logging_message(f"ALBUS ran successfully ")