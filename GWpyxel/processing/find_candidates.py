import numpy as np
from scipy import ndimage as ndi

import logging
logger = logging.getLogger(__name__)


_USE_INI_DEFAULT = object()

from GWpyxel.utils import time_logger
from GWpyxel.utils import set_logging_message
from GWpyxel.utils import regionprops_to_df

@time_logger
def find_candidates(self):

    mask = self.threshold_map(self.TFmap)


    self.regions_test = mask


    mask_glitch = self.threshold_map(self.Glitch)

    autreTFmap = np.zeros_like(self.TFmap)
    autreTFmap[mask] = self.TFmap[mask]

    autreGLITCHmap = np.zeros_like(self.TFmap)
    autreGLITCHmap[mask_glitch] = self.Glitch[mask_glitch]


    map_diff = autreTFmap-autreGLITCHmap

    

    #mask = erosion(mask,footprint=disk(1))
    from skimage.measure import label, regionprops, regionprops_table
    import math
    from skimage.morphology import skeletonize, thin
    from skimage.morphology import medial_axis, skeletonize
    skeleton= mask#skeletonize(mask)

    #skeleton= thin(mask, max_num_iter=5)
    #skeleton = medial_axis(mask, return_distance=False)


    label_img = label(skeleton)
    regions = regionprops(label_img,intensity_image=self.TFmap)

    self.skeleton = np.zeros_like(self.TFmap)
    for region in regions:
        label_mask = np.zeros_like(self.TFmap)
        for c in region.coords:
            label_mask[c[0], c[1]] = 1  # L1_spectrogram[c[0], c[1]]
        from skimage.morphology import (erosion, dilation, opening, closing,  # noqa
                        white_tophat)
        from skimage.morphology import disk 
        #self.skeleton += label_mask
        #label_mask = dilation(label_mask,footprint=disk(5))
        mask = ndi.distance_transform_edt(~label_mask.astype(bool),sampling=[0.2*self.config.tfmap.fres,0.7*self.config.tfmap.tres])
        #mask = ndi.distance_transform_edt(~label_mask.astype(bool),sampling=[0.7*self.config.tfmap.fres,0.7*self.config.tfmap.tres])
        mask = mask <= 5
        gros_label_mask = mask
        self.skeleton += gros_label_mask
        
        label_mask = label_mask.astype(bool)

        if hasattr(self, "simulation"):
            if np.sum(self.simulation.mask & np.swapaxes(gros_label_mask,0,1)) >= 5:
                setattr(region, f"injection", True)
            else:
                setattr(region, f"injection", False)

            region.sim_waveform = self.simulation.waveform
            region.sim_hrss = self.simulation.hrss
            region.sim_ra = self.simulation.right_ascension
            region.sim_dec = self.simulation.declination
            region.sim_polarization = self.simulation.polarization
            region.sim_delay = self.simulation.delay
            for ifo in self.ifos:
                setattr(region, f"SNR_{ifo}", self.simulation.SNR[ifo])

            setattr(region, f"SNR_network", self.simulation.SNR["network"])


        setattr(region, f"anomaly_score", np.sum(self.TFmap[gros_label_mask]))
        setattr(region, f"glitch_score", np.sum(self.Glitch[gros_label_mask]))
        setattr(region, f"diff_score", np.sum(map_diff[gros_label_mask]))
        with np.errstate(divide='ignore'):
            tmp = np.divide(self.TFmap[label_mask],self.Glitch[label_mask])
            tmp[np.isnan(tmp)] = 0
            tmp[tmp == np.inf] = 0
            setattr(region, f"rapport_score", np.mean(tmp))


        setattr(region, f"freq_start", region.bbox[0] * self.spectrogram.dy.value)
        setattr(region, f"freq_end", region.bbox[2] * self.spectrogram.dy.value)
        setattr(region,f"freq_peak",region.weighted_centroid[0] * self.spectrogram.dy.value)
        setattr(region,f"bandwidth",getattr(region, "freq_end") - getattr(region, "freq_start"))

        setattr(region, f"uniqueID", self.uuid)

        for ifo in self.ifos:
            setattr(region, f"GPS_block_start_{ifo}", self.data[ifo].t0.value)
            setattr(region,f"GPS_block_end_{ifo}",getattr(region, f"GPS_block_start_{ifo}")+ self.data[ifo].duration.value,)
            setattr(region,f"GPS_start_{ifo}",getattr(region, f"GPS_block_start_{ifo}")+ region.bbox[1] * self.spectrogram.dx.value,)
            setattr(region,f"GPS_end_{ifo}",getattr(region, f"GPS_block_start_{ifo}")+ region.bbox[3] * self.spectrogram.dx.value,)
            setattr(region,f"GPS_peak_{ifo}",getattr(region, f"GPS_block_start_{ifo}")+ region.weighted_centroid[1] * self.spectrogram.dx.value,)

        setattr(region,f"duration",getattr(region, f"GPS_end_{ifo}") - getattr(region, f"GPS_start_{ifo}"),)

        setattr(region,f"significance",                        (region.intensity_mean**2)
                    * region.eccentricity
                    * region.area
                    * region.convex_area
                    / (region.extent))
    self.candidates = regionprops_to_df(regions)
    self.regions = regions

    #self.candidates = self.candidates[self.candidates.duration > self.config.tfmap.tres]
   