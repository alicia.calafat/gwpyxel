import numpy as np
from termcolor import colored
import time
from gwpy.timeseries import TimeSeries
import os
from GWpyxel.const import PATH_TO_PYXEL
import pickle
import GWpyxel.utils
import logging
import copy

import logging
logger = logging.getLogger(__name__)


_USE_INI_DEFAULT = object()

from GWpyxel.utils import time_logger
from GWpyxel.utils import set_logging_message
@time_logger
def fetch_data(self,ifo, start=_USE_INI_DEFAULT, 
                    duration=_USE_INI_DEFAULT, 
                    verbose=_USE_INI_DEFAULT, 
                    cache=_USE_INI_DEFAULT, 
                    Fs=_USE_INI_DEFAULT,
                    source=_USE_INI_DEFAULT):
    # ---- Set default value from ini, couldn't find a way to mofidy variable in 
    #      a loop within the function. (Don't use locals()!)
    start = float(start) if (start != _USE_INI_DEFAULT) and isinstance(start, (int, float)) else start
    duration = float(self.config.data.duration) if duration == _USE_INI_DEFAULT else float(duration)
    if verbose is _USE_INI_DEFAULT: verbose = self.config.data.verbose
    if cache is _USE_INI_DEFAULT: cache = self.config.data.cache
    if Fs is _USE_INI_DEFAULT: Fs = self.config.data.Fs
    if source is _USE_INI_DEFAULT: source = self.config.data.source

    self.ifos = np.unique(np.append(self.ifos, ifo))

    # Fetch data based on different cases (PSD / GWOSC / MDC / LIGO) 
    if isinstance(start,str):

        from pycbc.noise import noise_from_string

        noise = noise_from_string(start,length= duration * Fs, delta_t=1 / Fs, seed=np.random.randint(1e8))
        self.data[ifo] = TimeSeries.from_pycbc(noise)
        self.data.channel = start
        self.data.run = 'PSD'
        self.data.frames = 'PSD'

        set_logging_message(f"Data generated from PSD {start}")

    else:
        self.data.run = GWpyxel.utils.run_at_gps(start)

        # Now, we set up the channel and frame on a case-per-case basis
        if source.upper() == 'GWOSC':
            self.data.channel = 'GWOSC'
            self.data.frames = 'GWOSC'
            logger.info(f"\t\t  Data frames:channel is {self.data.frames}:{self.data.channel}")
        elif source.split(':')[0].upper() == 'MDC':
            self.data.channel = f"{ifo}:" + source.split(':')[1]
            self.data.frames = 'MDC'
            logger.info(f"\t\t  Data frames:channel is {self.data.frames}:{self.data.channel}")
        elif source.upper() == 'LIGO':
            import configparser
            obsRun = configparser.ConfigParser()
            obsRun.read(f"{PATH_TO_PYXEL}/runs/{self.data.run}.ini")
            self.data.channel = obsRun.get(ifo, "channel") 
            self.data.frames = obsRun.get(ifo, "frames")
            logger.info(f"\t\t  Data frames:channel is {self.data.frames}:{self.data.channel}")
        else:
            logger.critical(f"\t  Data source not recognised: {source}")

        # Check if cache file exists
        fname = os.path.normpath(f"{self.config.cache.path}/{self.data.run}/{ifo}/{self.data.run}-{self.data.frames}-{self.data.channel}-{start}-{duration}-{Fs}.pkl")
        if cache and os.path.isfile(fname):
            with open(fname, "rb") as f:
                self.data[ifo] = pickle.load(f)

            set_logging_message(f"Read from cache: {fname}")

        
        else:
            if self.data.frames == 'GWOSC':

                self.data[ifo] = TimeSeries.fetch_open_data(ifo, start, start + duration,verbose=verbose)
                set_logging_message(f"Read from GWOSC")

            elif self.data.frames == 'MDC':
                self.data[ifo] = TimeSeries.read(f'{self.config.MDC.path}/{ifo}.cache', f'{self.data.channel}', start=start, 
                                                end=start+duration)
                set_logging_message(f"Read from MDC: {self.data.channel}")

            else:
                # Had to remove this due to Dan Kozak email: bug in the configuration files on their side so was awlays using TAPE.
                # do not know when they will fix it.
                #try:
                #    from pycbc import frame
                #    self.data[ifo] = TimeSeries.from_pycbc(frame.query_and_read_frame(self.data.frames, 
                #                                            self.data.channel, 
                #                                            start,start+duration))
                #    set_logging_message(f"Fetched from frames: {self.data.frames} {self.data.channel}")

                #except:
                self.data[ifo] = TimeSeries.get(self.data.channel,start,start + duration,allow_tape=False,
                verbose=verbose)  

                set_logging_message(f"Fetched from GWPY: {self.data.channel}")


            # Resample to desired rate
            if float(np.round(self.data[ifo].sample_rate.value,2)) != float(np.round(Fs,2)):
                old_sample_rate = float(np.round(self.data[ifo].sample_rate.value,2))
                start_time = time.time()
                self.data[ifo] = self.data[ifo].resample(Fs)
                end_time = time.time()
                elapsed_time = end_time - start_time

                logger.info(f"\t ({elapsed_time:.3f}s)  Resampled from {old_sample_rate} to {Fs}")

            # write cache file if it does not exist
            if cache and not os.path.isfile(fname):
                if self.data.frames != 'PSD':
                    start_time = time.time()
                    
                    with open(fname, "wb") as f:
                        pickle.dump(self.data[ifo], f)
                    end_time = time.time()
                    elapsed_time = end_time - start_time

                    logger.info(f"\t ({elapsed_time:.3f}s)  Saved to cache {fname}")
                    
        # Make a copy of the data for book keeping later on. 
        # This could be optimise, Vincent used _untouched when training ALBUS
        # But there surely is a better way. This increase memory consumption. 
        self.data_original[ifo] = copy.deepcopy(self.data[ifo])
        self.data_untouched[ifo] = copy.deepcopy(self.data[ifo])

        # Check if all t0.value are the same or different
        t0_values = [self.data[ifo].t0.value for ifo in self.ifos]
        are_all_same = len(set(t0_values)) == 1
        
        # Set up a name to save the file
        prefix = 'foreground_' if are_all_same else 'background_'
        self.name = prefix + "_".join(
            map(
                "{0}-{1}".format,
                self.ifos,
                [int(value) for value in t0_values],
            )
        )

        self.name += f"_{self.uuid}"
    
    
