
import logging
logger = logging.getLogger(__name__)
import matplotlib.pyplot as plt
import numpy as np

_USE_INI_DEFAULT = object()

from GWpyxel.utils import time_logger
from GWpyxel.utils import set_logging_message
@time_logger  
def save_plot_albus(self,version=2,output=''):
    fig = plt.figure(frameon=False)
    if version ==1 :
        fig.set_size_inches(1, 6.175)
    elif version == 2:
        fig.set_size_inches(1, 3.091)
    else:
        fig.set_size_inches(1, 3.091)
        print('version warning')
    ax = plt.Axes(fig, [0.0, 0.0, 1.0, 1.0])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax.imshow(
        np.swapaxes(self.spectrogram.value, 0, 1),
        origin="lower",
        aspect="auto",
        cmap="cubehelix",
        vmin=0,
        vmax=np.percentile(self.spectrogram.value, 99.5),
    )
    if output == '':
        plt.savefig(f"{self.name}.png", dpi=166)
    else:
        plt.savefig(f"{output}/{self.name}.png", dpi=166)
    plt.close()