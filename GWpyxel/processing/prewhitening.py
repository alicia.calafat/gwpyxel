import numpy as np
import scipy.signal
import matplotlib.pyplot as plt
from GWpyxel.utils import time_logger
from GWpyxel.utils import set_logging_message

import logging
logger = logging.getLogger(__name__)

_USE_INI_DEFAULT = object()


@time_logger
def prewhitening(self,fftlen=_USE_INI_DEFAULT,method='single'):
    if fftlen is _USE_INI_DEFAULT: fftlen = self.config.whitening.fftlen

    for ifo in self.ifos:
        ASD = self.data[ifo].asd(fftlen)
        if method.lower() == "single":
            self.data[ifo] = self.data[ifo].whiten(asd=ASD)
        elif method.lower() == "double":
            self.data[ifo] = self.data[ifo].whiten(asd=ASD).whiten(asd=ASD)
        else:
            pass
    set_logging_message(f"Data whitened")


