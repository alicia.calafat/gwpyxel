import logging
import time
import numpy as np
import GWpyxel.utils
from skimage.transform import rescale, resize
from skimage.filters import threshold_mean

import logging
logger = logging.getLogger(__name__)


_USE_INI_DEFAULT = object()

from GWpyxel.utils import time_logger
from GWpyxel.utils import set_logging_message
@time_logger
def generate_tfmap(self, prewhitening=_USE_INI_DEFAULT, postwhitening=_USE_INI_DEFAULT):
    # ---- Set default value from ini, couldn't find a way to mofidy variable in 
    #      a loop within the function. (Don't use locals()!)
    if prewhitening is _USE_INI_DEFAULT: prewhitening = self.config.whitening.prewhitening
    if postwhitening is _USE_INI_DEFAULT: postwhitening = self.config.whitening.postwhitening

    #Set name of stuff here
    if not hasattr(self,'name'):
        self.name = "_".join(
            map(
                "{0}-{1}".format,
                self.ifos,
                [int(self.data[ifo].t0.value) for ifo in self.ifos],
            )
        )
        self.name += f"_{self.uuid}"


    if prewhitening:
        for ifo in self.ifos:
            ASD = self.data[ifo].asd(self.config.whitening.fftlen)
            if prewhitening.lower() == "single":
                self.data[ifo] = self.data[ifo].whiten(asd=ASD)
            elif prewhitening.lower() == "double":
                self.data[ifo] = self.data_original[ifo].whiten(asd=ASD).whiten(asd=ASD)
            else:
                pass
    
    if len(self.ifos) == 1:
        self.spectrogram = self.data[self.ifos[0]].spectrogram(
            self.config.tfmap.tres,
            fftlength=1 / self.config.tfmap.fres,
            overlap=np.round(
                (1 / self.config.tfmap.fres) * self.config.tfmap.poverlap / 100, 2
            ),
            method='welch'
        )
        self.spectrogram.update(np.log(self.spectrogram.value))

    if len(self.ifos) == 2:
        self.spectrogram = self.data[self.ifos[0]].coherence_spectrogram(
            self.data[self.ifos[1]],
            self.config.tfmap.tres,
            fftlength=1 / self.config.tfmap.fres,
            overlap=np.round(
                (1 / self.config.tfmap.fres) * self.config.tfmap.poverlap / 100, 2
            ),
        )

    set_logging_message(f"TFmap generated")

    if postwhitening:
        whitening_factor = self.spectrogram.value.sum(axis=0)[None, :]
        whitening_factor[np.where(whitening_factor == 0)] = np.inf
        self.spectrogram = self.spectrogram/(whitening_factor)

    if hasattr(self,'simulation'):
        # make own function here and use timer 
        spec = self.simulation.mask_ht.spectrogram2(self.config.tfmap.tres)
        supermask = spec.value >= 0.1*threshold_mean(spec.value)
        supermask = resize(supermask, (self.spectrogram.shape[0], self.spectrogram.shape[1]),
                               anti_aliasing=False,preserve_range=True)

        psd_map,f = self.compute_psd(self.simulation.mask_ht.value)
        mask = psd_map>1e-48

    
        self.simulation.mask = np.swapaxes(mask,0,1)
        self.find_injection_mask()
        



